# Chrome Weather Extension

#### A Weather Forecast Extension for Chrome Browser. Develop as a final project in Introduction to IoT, NCTUCS 2017 Fall.

## 0. Installation

> Since recent versions of Chrome no longer support installing packed extensions from unknown source. The only way to use an extension not from Chrome Store is sideloading the source code manually.

> This extension is developed on Chrome Version 63.0.3239.132 (Official Build) (64-bit), so running on Chrome Version 63+ is recommended.

1. Run `npm install` inside the project folder to install dependent Javascript libraries from NPM.

2. Open Chrome, go to extensions management page with URL `chrome://extensions`.

3. Check `Developer mode` to enable more options.

4. Click on `Load unpacked extension` ，choose the extension folder `weather/` (the one with a `manifest.json`).

5. If the extension is loaded successfully, there will be an icon `W` at top-right of Chrome.

6. Click the icon to open extension，by default it displays weather forecast of New Taipei City.

> Currently, the extension retrieves data from `weather.nctu.me`, where we deploy the final version of `Weather-backend`. If you would like to host your own `Weather-backend`, please modify all urls of API requests in this project to your domain or ip address.

## 1. Introduction

### Folder Structure

```bash
weather
├── app.html
├── app.js
├── background.html
├── background.js
├── components
│   ├── settings.js
│   ├── weather-chart.js
│   └── weather-icon.js
├── css
│   ├── style.css
│   ├── typeahead.css
│   └── weather-icon.css
├── manifest.json
├── node_modules
├── package.json
├── README.md
├── sample.json
└── vendor
    ├── lodash.js
    ├── typeahead.jquery.js
    └── vue-csp.js
```

### Description

* `app.html` :  The GUI template of the extension. Using `Vue.js` for data rendering and user input control, `Milligram.css`for UI theme.

* `app.js` : The core script containing all control logic for `app.html`.

* `background.html` `background.js` : Containing code that needs to run even if a user is not using the extension. Currently only for Pop-up Notification.

* `components/` : Custom Vue.js components used in `app.html`.

  * `settings.js` : Render settings page and save user changes with `chrome.storage` API.

  * `weather-chart.js` : Render rain possibility and temperature forecast chart. Using `Chart.js`.

  * `weather-icon.js` : Weather icon controller component. Render an SVG icon by giving an icon id. Using icons from [codepen.io](https://codepen.io/anon/pen/EowrXM).

* `css/` : CSS file for custom UI style.

  * `style.css` : Custom colors, fonts, and HTML element sizes.

  * `typeahead.css` : Styles for `typeahead.js` library.

  * `weather-icon.css` : Styles for weather icons. From [codepen.io](https://codepen.io/anon/pen/EowrXM).

* `manifest.json` : Config file of this chrome extension. Used by Chrome to load the extension.

* `node_modules/` : Contain Javascript libraries that are installed from NPM.

* `package.json` : NPM config file.

* `sample.json` : A sample JSON data from `Weather-backend` by `JevanChan`

* `vendor/` : Contain Javascript libraries that are not installed from NPM. Note that `vue-csp.js` is used instead of normal Vue runtime due to `Content Security Policy` limitation in Chrome Extension API.

## 2. Application Manual

### Search Field

  * One can search for any county(縣市) and town(鄉鎮市區) in Taiwan, and choose the interesting place in the hints list，then click on `前往` to view weather forecast of the chosen place.

### Weather Panel

  * The center shows today's weather, click to view the chart of rain possibility and temperature forecast.
  * Rain possibility has less data because Central Weather Bureau offer less.
  * The Bottom shows forecast in 7 days，click right or left arrow button or scroll touchpad to view all.

### Settings

  *  `所在地`: Selected location is automatically saved and displayed next time the extension is opened. _Application sets `所在地` to current user location at first use.（using HTML5 geolocation API）_
  *  `定位`: Click `GO` to set `所在地` to current user location.（using HTML5 geolocation API）
  *  `降雨機率通知`: ON/OFF takes effect immediately. Notification interval needs restarting Chrome or reloading extension to take affect. To make testing easier, the interval is shortened. (1hr => 30s)

## Authors:

  * dragonman225 (0510119 王文治)
  * JevanChan (0310134 詹鈞年)
