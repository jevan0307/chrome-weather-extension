// weather-chart component
Vue.component('weather-chart', {
  props: ['chart'],
  template: [
    '<h4></h4>',
    '<canvas id="weatherChart" width="400" height="300"></canvas>'
  ].join(''),
  methods: {
    render: function() {
      var ctx = document.getElementById("weatherChart").getContext("2d");
      var myChart = new Chart(ctx, chartOptions);
    }
  },
  ready: function() {
    console.log(this.chart);
    chartOptions.data.labels = this.chart.time;
    chartOptions.data.datasets[0].data = this.chart.rain;
    chartOptions.data.datasets[1].data = this.chart.temp;
    this.render();
  }
});

var chartOptions = {
  type: 'line',
  data: {
    labels: [],
    datasets: [{
        label: '降雨機率(％)',
        yAxisID: 'rain',
        data: [],
        backgroundColor: [
          'rgba(81, 212, 250, 0.2)'
        ],
        borderColor: [
          'rgba(3,169,244,1)'
        ],
        borderWidth: 1
      },
      {

        label: '溫度(℃)',
        yAxisID: 'temp',
        data: [],
        backgroundColor: [
          'rgba(255, 224, 82, 0.2)'
        ],
        borderColor: [
          'rgba(255,193,7,1)'
        ],
        borderWidth: 1
      }
    ]
  },
  options: {
    scales: {
      yAxes: [{
          id: 'rain',
          position: 'left',
          ticks: {
            max: 100,
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: '降雨機率(％)'
          }
        },
        {
          id: 'temp',
          position: 'right',
          ticks: {
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: '溫度(℃)'
          }
        }
      ]
    }
  }
}
