let notificationInterval = 1;
let multiplier = 30000;
let geocode = 65;
let geoname = '新北市';
let last = null;
let enable = true;

document.addEventListener('DOMContentLoaded', function() {
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.');
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

function notifyMe(msg) {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification('天氣小提醒', {
      body: msg,
    });
    console.log(notification);

    if (last !== null)
      last.close()
    last = notification

    notification.onclick = function() {
      notification.close();
    };
  }
}

chrome.storage.local.get(['userLocation', 'notiConfig'], function(item) {
    notificationInterval = item.notiConfig.rain.interval*multiplier;
    geocode = item.userLocation.geocode;
    geoname = `${item.userLocation.city} ${item.userLocation.area}`;
    enable = item.notiConfig.rain.enable;
});

chrome.storage.onChanged.addListener(function(changes, namespace) {
  for (key in changes) {
    var storageChange = changes[key];
    if (key === 'userLocation') {
      geocode = storageChange.newValue.geocode;
      geoname = `${storageChange.newValue.city} ${storageChange.newValue.area}`;
    }
    else if (key === 'notiConfig') {
      notificationInterval = storageChange.newValue.rain.interval*multiplier;
      enable = storageChange.newValue.rain.enable;
    }
    console.log('Storage key "%s" in namespace "%s" changed. ' +
      'Old value was "%s", new value is "%s".',
      key,
      namespace,
      storageChange.oldValue,
      storageChange.newValue);
  }
});

// Start Notification
var weatherNotification = setInterval(function() {
  axios.get('http://weather.nctu.me/weather/7days?geocode=' + geocode)
    .then((res) => {
      console.log(res);
      console.log(notificationInterval);
      if (enable) notifyMe(`${geoname} 降雨機率：${res.data.content.PoP[0].value}%`);
    })
    .catch((err) => {
      console.log(err);
    });
}, notificationInterval * multiplier);
