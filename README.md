# Chrome Weather Extension
The final project for Introduction to IoT, 2017 Fall, NCTUCS.  

## ** Important Notification**
For the demo of final project of Introduction to IoT, our server will be held to serve the weather and geolocation APIs until Feb 15, 2018. If you want to run this extension after then, please run the web backend on your own host and modify the URL of APIs in extension frontend.

## Description
This is a weather forecast chrome extension powered by Central Weather Bureau. We use CWB Open Data, Google Maps API, and Google Place API to serve our product. Our product has the ability to provide weather forecast of each county/city/town/township in Taiwan in next seven days, locating users, and there is also a search bar for users to type in whatever place they want to see the forecast.  
For more details, please refer to our presentation slides in class:
* [IoT Final Project - Chrome Weather Extension](Chrome_Weather_Extension.pdf)

## Source Code
This project is separated into two parts, extension frontend and web backend. The extension is programmed in web programming languages with Vue.js frontend framework, and the backend is programmed in Node.js.
Please refer to directory `Weather` for the extension frontend and `Weather-backend` for the web-backend.  
For more details, please read the `README.md` in each directory.

## Contribution
What follows are the members of our group of final project:
* [詹鈞年 (0310134)](mailto:jevan0307@gmail.com)
    * Idea
    * Extension frontend
    * Web backend (main)
    * Slide
* [王文治 (0510119)](mailto:dragonman.ee05@nctu.edu.tw)
    * Idea
    * Extension frontend (main)
    * Web backend
    * Slide & Presentation
* [梁晉祥 (0310112)](mailto:grinnq30332@gmail.com)
    * Idea
    * Slide & Presentation
* [謝亞修 (0310126)](mailto:simon10030950@gmail.com)
    * Idea
    * Slide & Presentation
* [郭宗儀 (0416064)](mailto:jash4088@gmail.com)
    * Slide & Presentation
