export default {
    googleMap: {
        // Set API key of Google Maps API here
        key: '',
        rGeocodingUrl: 'https://maps.googleapis.com/maps/api/geocode/json'
    },
    googlePlace: {
        // Set API key of Google Place API here
        key: '',
        placeAutoCompleteUrl: 'https://maps.googleapis.com/maps/api/place/autocomplete/json'
    },
    cwb: {
        // Set API key of CWB Open Data here
        key: '',
        url: 'http://opendata.cwb.gov.tw/opendataapi',
        dataid: {
            'county2days': 'F-D0047-089',
            'county7days': 'F-D0047-091',
            'town2days': {
                '65': 'F-D0047-069',
                '10014': 'F-D0047-037',
                '10020': 'F-D0047-057',
                '10007': 'F-D0047-017',
                '10004': 'F-D0047-009',
                '10008': 'F-D0047-021',
                '09020': 'F-D0047-085',
                '10015': 'F-D0047-041',
                '63': 'F-D0047-061',
                '10016': 'F-D0047-045',
                '10010': 'F-D0047-029',
                '10002': 'F-D0047-001',
                '68': 'F-D0047-005',
                '10009': 'F-D0047-025',
                '64': 'F-D0047-065',
                '10018': 'F-D0047-053',
                '10017': 'F-D0047-049',
                '66': 'F-D0047-073',
                '10013': 'F-D0047-033',
                '67': 'F-D0047-077',
                '09007': 'F-D0047-081',
                '10005': 'F-D0047-013'
            },
            'town7days': {
                '65': 'F-D0047-071',
                '10014': 'F-D0047-039',
                '10020': 'F-D0047-059',
                '10007': 'F-D0047-019',
                '10004': 'F-D0047-011',
                '10008': 'F-D0047-023',
                '09020': 'F-D0047-087',
                '10015': 'F-D0047-043',
                '63': 'F-D0047-063',
                '10016': 'F-D0047-047',
                '10010': 'F-D0047-031',
                '10002': 'F-D0047-003',
                '68': 'F-D0047-007',
                '10009': 'F-D0047-027',
                '64': 'F-D0047-067',
                '10018': 'F-D0047-055',
                '10017': 'F-D0047-051',
                '66': 'F-D0047-075',
                '10013': 'F-D0047-035',
                '67': 'F-D0047-079',
                '09007': 'F-D0047-083',
                '10005': 'F-D0047-015'
            }
        }
    }
}
