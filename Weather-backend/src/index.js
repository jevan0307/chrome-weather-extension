import { cwb, googleMap, geocode, googlePlace } from '~/api'
import { server } from '~/config'
import * as express from 'express'
import * as request from 'superagent'
import * as cors from 'cors'

let app = express()

app.use(cors())

app.get('/', async (req, res) => {
    res.send({ message: 'OK' })
})

app.get('/weather/7days', async (req, res) => {
    res.send(await cwb.weather7days(req.query.geocode))
})

app.get('/weather/2days', async (req, res) => {
    res.send(await cwb.weather2days(req.query.geocode))
})

app.get('/geolocation/geocode', async (req, res) => {
    res.send(geocode.find(req.query.input))
})

app.get('/geolocation/autocomplete', async (req, res) => {
    res.send(await googlePlace.placeAutoComplete(req.query.input))
})

app.get('/geolocation/rgeocoding', async (req, res) => {
    if (req.query.place_id)
        res.send(await googleMap.reverseGeocodingByPlaceId(req.query.place_id))
    else
        res.send(await googleMap.reverseGeocoding(req.query.lat, req.query.lng))
})

app.listen(server.port, server.domain, async (err) => {
    if (err) {
        console.error(err)
        throw (err)
    }
    console.log(`Server is listening on ${server.domain}:${server.port}`)
})
