import { api } from '~/config'
import * as request from 'superagent'

let config = api.googlePlace

let placeAutoComplete = async (input) => {
    if (!input)
        return { predictions: [] }

    try {
        let res = await request
            .get(config.placeAutoCompleteUrl)
            .query({ input })
            .query({ key: config.key })
            .query({ types: 'geocode' })
            .query({ language: 'zh-TW' })

        let data = res.body['predictions']

        return {
            predictions: data.map((pred) => {
                return {
                    description: pred.description,
                    place_id: pred.place_id
                }
            })
        }
    } catch (e) {
        return { predictions: [] }
    }
}

export {
    placeAutoComplete
}
