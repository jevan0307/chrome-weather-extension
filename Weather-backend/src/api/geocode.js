import { geocode } from '~/config'

let find = (str) => {
    let county = null
        , town = null

    if (!str)
        return []

    let candidates = Object.keys(geocode)
        , matchIdx = 0
    str.split('').forEach((c) => {
        if (candidates.length == 0 || (town && county))
            return
        if ('\n\t\r\n+ '.split('').findIndex((ec) => ec == c) > 0)
            return

        candidates = candidates.filter((item) => item[matchIdx] == (c == '台' ? '臺' : c))

        ++matchIdx
        if (candidates.length == 1 && matchIdx == candidates[0].length) {
            if (!county) {
                county = candidates[0]
                candidates = Object.keys(geocode[county]) || []
                matchIdx = 0
            } else {
                town = candidates[0]
                candidates = []
            }
        }
    })

    if (town)
        candidates.push(town)

    return candidates.map((name) => {
        return {
            location: str + (name == '_' ? '' : name.substring(matchIdx)),
            geocode: (county ? geocode[county][name] : geocode[name]['_'])
        }
    })
}

export {
    find
}
