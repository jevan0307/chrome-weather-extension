import { api } from '~/config'
import * as request from 'superagent'

let config = api.googleMap

let reverseGeocoding = async (lat, lng) => {
    if (!lat || !lng)
        return { error: "Invalid input" }
    try {
        let res = await request
            .get(config.rGeocodingUrl)
            .query({ latlng: `${lat.trim()},${lng.trim()}` })
            .query({ key: config.key })
            .query({ language: 'zh-TW' })



        let addrComponents = res.body.results[0].address_components
        let country = addrComponents.find(
            (item) => item.types.findIndex((t) => t == "country") >= 0
        ).long_name

        if (country !== '台灣' && country !== '臺灣')
            return { error: "Not in Taiwan" }

        return {
            formatted_address: res.body.results[0].formatted_address,
            county: (addrComponents.find(
                (item) => {
                    if (item.long_name === '台灣省') {
                        return false;
                    } else {
                        return item.types.findIndex((t) => t=="administrative_area_level_2" || t=="administrative_area_level_1") >= 0
                    }
                }
            ) || { long_name: "_" }).long_name,

            town: (addrComponents.find(
                (item) => item.types.findIndex((t) => t == "administrative_area_level_3") >= 0
            ) || { long_name: "_" }).long_name,

            country: addrComponents.find(
                (item) => item.types.findIndex((t) => t == "country") >= 0
            ).long_name
        }
    } catch (e) {
        return { error: "Fetch data failed" }
    }
}

let reverseGeocodingByPlaceId = async (place_id) => {
    if (!place_id)
        return { error: "Invalid input" }
    try {
        let res = await request
            .get(config.rGeocodingUrl)
            .query({ place_id })
            .query({ key: config.key })
            .query({ language: 'zh-TW' })


        let addrComponents = res.body.results[0].address_components
        let country = addrComponents.find(
            (item) => item.types.findIndex((t) => t == "country") >= 0
        ).long_name

        if (country !== '台灣' && country !== '臺灣')
            return { error: "Not in Taiwan" }

        return {
            formatted_address: res.body.results[0].formatted_address,
            county: (addrComponents.find(
                (item) => {
                    if (item.long_name === '台灣省') {
                        return false;
                    } else {
                        return item.types.findIndex((t) => t=="administrative_area_level_2" || t=="administrative_area_level_1") >= 0
                    }
                }
            ) || { long_name: "_" }).long_name,

            town: (addrComponents.find(
                (item) => item.types.findIndex((t) => t == "administrative_area_level_3") >= 0
            ) || { long_name: "_" }).long_name,

            country: addrComponents.find(
                (item) => item.types.findIndex((t) => t == "country") >= 0
            ).long_name
        }
    } catch (e) {
        return { error: "Fetch data failed" }
    }
}


export {
    reverseGeocoding,
    reverseGeocodingByPlaceId
}
