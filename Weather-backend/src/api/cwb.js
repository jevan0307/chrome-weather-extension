import { api } from '~/config'
import * as convert from 'xml-js'
import * as request from 'superagent'

let config = api.cwb

let weather7days = async (geocode = null) => {
    let countyGeoCode = null
        , townGeoCode = null

    if (!geocode)
        return { error: "Invalid input" }

    countyGeoCode = Object.keys(config.dataid['town7days']).find((gc) => geocode.indexOf(gc) == 0)

    if (!countyGeoCode)
        return {}
    if (countyGeoCode !== geocode)
        townGeoCode = geocode

    let res
    try {
        res = await request
            .get(config.url)
            .query({ dataid: townGeoCode ? config.dataid['town7days'][countyGeoCode] : config.dataid['county7days'] })
            .query({ authorizationkey: config.key })
            .buffer()

        res = convert.xml2js(res.text, {
            compact: true,
            trim: true,
        })
    } catch (e) {
        return { error: "Fetch data failed" }
    }

    if (townGeoCode)
        res = res['cwbopendata']['dataset']['locations']['location']
    else
        res = res['cwbopendata']['dataset']['location']

    let location = res.find((loc) => loc.geocode._text === geocode)
    if (!location)
        return {}
    let content = location.weatherElement.map((wElement) => {
        let name = wElement.elementName._text

        switch (name) {
            case 'T':
            case 'MaxT':
            case 'MinT':
            case 'PoP':
                wElement.time.forEach((time, i) => {
                    wElement.time[i] = {
                        "time": time.startTime ? time.startTime._text : time.dataTime._text,
                        "value": parseInt(time.elementValue.value._text),
                        "measures": time.elementValue.measures._text
                    }
                })
                return { [name]: wElement.time }
            case 'Wx':
                wElement.time.forEach((time, i) => {
                    wElement.time[i] = {
                        "time": time.startTime ? time.startTime._text : time.dataTime._text,
                        "value": time.elementValue.value._text,
                        "parameter": parseInt(time.parameter.parameterValue._text)
                    }
                })
                return { [name]: wElement.time }
            default:
                return {}
        }
    }).reduce((content, wElement) => Object.assign(content, wElement), {})


    return {
        location: location.locationName._text,
        geocode: location.geocode._text,
        content: content
    }
}

let weather2days = async (geocode = null) => {
    let countyGeoCode = null
        , townGeoCode = null

    if (!geocode)
        return { error: "Invalid input" }

    countyGeoCode = Object.keys(config.dataid['town2days']).find((gc) => geocode.indexOf(gc) == 0)

    if (!countyGeoCode)
        return {}
    if (countyGeoCode !== geocode)
        townGeoCode = geocode

    let res
    try {
         res = await request
            .get(config.url)
            .query({ dataid: townGeoCode ? config.dataid['town2days'][countyGeoCode] : config.dataid['county2days'] })
            .query({ authorizationkey: config.key })
            .buffer()

        res = convert.xml2js(res.text, {
            compact: true,
            trim: true,
        })
    } catch (e) {
        return { error: "Fetch data failed" }
    }


    if (townGeoCode)
        res = res['cwbopendata']['dataset']['locations']['location']
    else
        res = res['cwbopendata']['dataset']['location']

    let location = res.find((loc) => loc.geocode._text === geocode)
    if (!location)
        return {}

    let content = location.weatherElement.map((wElement) => {
        let name = wElement.elementName._text
        switch (name) {
            case 'T':
            case 'MaxT':
            case 'MinT':
            case 'PoP':
                wElement.time.forEach((time, i) => {
                    wElement.time[i] = {
                        "time": time.startTime ? time.startTime._text : time.dataTime._text,
                        "value": parseInt(time.elementValue.value._text),
                        "measures": time.elementValue.measures._text
                    }
                })
                return { [name]: wElement.time }
            case 'Wx':
                wElement.time.forEach((time, i) => {
                    wElement.time[i] = {
                        "time": time.startTime ? time.startTime._text : time.dataTime._text,
                        "value": time.elementValue.value._text,
                        "parameter": parseInt(time.parameter.parameterValue._text)
                    }
                })
                return { [name]: wElement.time }
            default:
                return {}
        }
    }).reduce((content, wElement) => Object.assign(content, wElement), {})

    return {
        location: location.locationName._text,
        geocode: location.geocode._text,
        content: content
    }
}

export {
    weather7days,
    weather2days
}
