# IoT Weather Extension Backend
This is the web backend for Chrome Weather Extension developed by dragonman225 in Introduction to IoT, NCTUCS 2017 Fall.

## Configuration
Configuration is necessary to be set before serving this backend. The configuration files are located in directory `src/config`

### PORT and DOMAIN
The port and domain listened by the server can be configured in `src/config/server.js`. By default, the configuration of the port is `3000` and domain is `127.0.0.1`.

### APIs
The API keys (authorization keys) are configured in `src/config/api.js`. Please refer to the following documents to apply for API keys and set them before serving the backend.  
In our project, we use Google Maps API, Google Place API, and Central Weather Bureau Open Data to provide our service. What follows are the documents of how to get the API keys:
* [Google Maps API](https://developers.google.com/maps/documentation/javascript/get-api-key)
* [Google Place API](https://developers.google.com/places/web-service/get-api-key)
* [CWB Open Data](https://opendata.cwb.gov.tw/usages)

Please read the terms of service and policy of the API provider. **DO NOT** violate the terms when using this backend as part of your project.

## How to use
This project is powered by Noes.js and ExpressJS. Make sure you install the latest version of Node.js in the host.

### Install
`$ npm install`
> Make sure to use the version of Node.js >= 9.0.0

### Run in development mode
`$ npm run dev`
### Build
`$ npm run build`  
The successfully built entry can be found as `build/main.js`

## Project Structure
The directory structure of this project is as follows:  
```
Weather Extension Backend
|-- src
    |-- api
        |-- cwb.js
        |-- geocode.js
        |-- google_map.js
        |-- google_place.js
        |-- index.js
    |-- config
        |-- api.js
        |-- geocode.js
        |-- server.js
        |-- index.js
    |-- index.js
|-- build
|-- backpack.config.js
|-- package.json
```
### Description
* `backpack.config.js`: The configuration for [Backpack](https://github.com/jaredpalmer/backpack), which is a minimalistic build system for Node.js.
* `build/`: All the files compiled by backpack with command `npm run build` will be placed in this directory.
* `src/`: The source code of this project
    * `src/index.js`: The entry point of this project. All the routers are also defined in this file.
    * `src/api/`: API related functions
        * `src/api/cwb.js`: Functions call the CWB Open Data API to get the weather forecast data.
        * `src/api/geocode.js`: Functions find the geocode from user's input. It is used to map user's input to geocode as well as provide candidates when the user is typing in the location search box.
        * `src/api/google_map.js`: Functions call the reverse geocoding API of Google Maps API. This is used to find the geocode from user's geolocation information, including longitude and latitude. Also, when frontend enables the advance location search, which is powered by Google Place API, it can look up reverse geocoding from Google Place ID.
        * `src/api/google_place.js`: Functions call the place autocomplete API of Google Place API. It is used to provide location candidates from user's input, and it is also an advance and optional feature for the frontend. The functions will return place name as well as place ID for each candidate, and this information can be used to look up geocoding.
    * `src/config/`: Configuration of the web backend
        * `src/config/api.js`: Configuration for the APIs, including API keys, and API URL and parameters.
        * `src/config/geocode.js`: Map from Taiwan's County/Town to Geocode. It is used to look up weather data from CWB Open Data platform.
        * `src/config/server.js`: Configuration of the server, including domain and port.

## Authors
* [Jevan Chan](mailto:jevan0307@gmail.com)
