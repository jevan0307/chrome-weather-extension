// backpack.config.js
let path = require('path')

module.exports = {
  webpack: (config, options, webpack) => {
    // Perform customizations to config
    // Important: return the modified config
    config.resolve.alias = {
        '~': path.resolve(__dirname, './src')
    }
    return config
  }
}
